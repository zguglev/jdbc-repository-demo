package services;

import pojo.User;
import repository.UserRepository;
import repository.specifications.UserSpecificationByAge;
import repository.specifications.UserSpecificationById;
import repository.specifications.UserSpecificationByUsername;

import java.util.List;

public class UserServiceImpl implements UserService<User> {


    private UserRepository<User> userRepository;

    public UserServiceImpl(UserRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findUserById(int id) {
        return userRepository.query(new UserSpecificationById(id)).get(0);
    }

    @Override
    public List<User> findAllUsersWithUsername(String username) {
        return userRepository.query(new UserSpecificationByUsername(username));
    }

    @Override
    public List<User> findAllUsersWithAge(int age) {
        return userRepository.query(new UserSpecificationByAge(age));
    }

    @Override
    public User create(User entity) {
        return userRepository.create(entity);
    }

    @Override
    public User update(User entity) {
        return userRepository.update(entity);
    }

    @Override
    public User delete(User entity) {
        return userRepository.delete(entity);
    }
}
