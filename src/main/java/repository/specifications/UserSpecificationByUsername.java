package repository.specifications;

import pojo.User;

import java.util.HashMap;
import java.util.Map;

public class UserSpecificationByUsername implements Specification<User> {

    private String username;

    public UserSpecificationByUsername(String username) {
        this.username = username;
    }

    public QueryInfo toQueryInfo() {
        QueryInfo info = new QueryInfo();
        //builder can be used here
        String sql = "SELECT * FROM user WHERE username = ?";
        Map<Integer, Object> placeholders = new HashMap<>();
        placeholders.put(1, username);
        info.setSql(sql);
        info.setPlaceholders(placeholders);
        return info;
    }
}
