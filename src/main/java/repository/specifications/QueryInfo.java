package repository.specifications;

import java.util.Map;

public class QueryInfo {

    private String sql;
    private Map<Integer, Object> placeholders;

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Map<Integer, Object> getPlaceholders() {
        return placeholders;
    }

    public void setPlaceholders(Map<Integer, Object> placeholders) {
        this.placeholders = placeholders;
    }
}
