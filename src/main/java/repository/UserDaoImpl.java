package repository;

import pojo.User;

public class UserDaoImpl implements UserDao<User> {

    @Override
    public User get(int id) {
        return null;
    }

    @Override
    public void create(User user) {

    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public User getByAge(int age) {
        return null;
    }

    @Override
    public User getByUsernameAndPassword(String username, String password) {
        return null;
    }
}
