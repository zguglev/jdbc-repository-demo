package repository;

public class SqlBuilder {

    //SELECT * FROM user WHERE id = 1;
    public SqlBuilder(String sql) {
        this.sql = sql;
    }

    private String sql;

    public SqlBuilder select(String table) {
        sql += "SELECT * FROM " + table;
        return new SqlBuilder(sql);
    }

    public SqlBuilder where() {
        sql += " WHERE";
        return new SqlBuilder(sql);
    }

    public SqlBuilder fields(String fieldName, String value) {
        sql += " " + fieldName + "= " + value;
        return new SqlBuilder(sql);
    }

    public String getSql() {
        return sql;
    }
}
